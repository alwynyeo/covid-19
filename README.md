# Covid-19 App

It's a personal project that monitors the current Covid-19 statistics across the world and news related to Covid-19.

### Technology Used:
* Swift
* UIKit
* Alamofire
* Realm
* Charts
* SafariServices
* SDWebImage

### Features:
* Monitor the Covid-19 statistics
* Read the latest news related to Covid-19
* Able to choose to view worldwide data or individual country
* Able to read news from different countries
* Supports offline mode for news
* Supports light mode, and dark mode
* Auto Layout (Programmatic)

### Images:
![](Images/light-home.png) ![](Images/dark-home.png) ![](Images/dark-home-seg.png)

![](Images/light-chart.png) ![](Images/dark-chart.png)

![](Images/light-news.png) ![](Images/dark-news.png)

![](Images/light-region.png) ![](Images/dark-region.png)
