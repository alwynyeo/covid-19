//
//  CaseResult.swift
//  Covid-19
//
//  Created by Alwyn Yeo on 26/03/2020.
//  Copyright © 2020 Alwyn Dev. All rights reserved.
//

// MARK: - Struct
struct CaseResult: Decodable {
    
    // MARK: - Declarations
    public private(set) var response: [CaseItem]?
}
