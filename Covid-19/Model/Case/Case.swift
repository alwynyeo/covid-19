//
//  Case.swift
//  Covid-19
//
//  Created by Alwyn Yeo on 26/03/2020.
//  Copyright © 2020 Alwyn Dev. All rights reserved.
//

// MARK: - Struct
struct Case: Decodable {
    
    // MARK: - Declarations
    public private(set) var new: String?
    public private(set) var active: Int?
    public private(set) var critical: Int?
    public private(set) var recovered: Int?
    public private(set) var total: Int?
}
