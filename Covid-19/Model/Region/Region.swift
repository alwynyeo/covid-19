//
//  Region.swift
//  Covid-19
//
//  Created by Alwyn Yeo on 30/06/2020.
//  Copyright © 2020 Alwyn Dev. All rights reserved.
//

// MARK: - Struct
struct Region {
    
    // MARK: - Declarations
    public private(set) var title: String?
    public private(set) var image: String?
    public private(set) var smallerImage: String?
    public private(set) var code: String?
    
}
