//
//  Regions.swift
//  Covid-19
//
//  Created by Alwyn Yeo on 30/06/2020.
//  Copyright © 2020 Alwyn Dev. All rights reserved.
//

// MARK: - Struct
struct Regions {
    
    // MARK: - Declarations
    var regions = [Region]()
    
    init() {
        regions.append(Region(title: "Argentina", image: "ar-60", smallerImage: "ar-30", code: "AR"))
        regions.append(Region(title: "Australia", image: "au-60", smallerImage: "au-30", code: "AU"))
        regions.append(Region(title: "Austria", image: "at-60", smallerImage: "at-30", code: "AT"))
        regions.append(Region(title: "Belgium", image: "be-60", smallerImage: "be-30", code: "BE"))
        regions.append(Region(title: "Brazil", image: "br-60", smallerImage: "br-30", code: "BR"))
        regions.append(Region(title: "Canada", image: "ca-60", smallerImage: "ca-30", code: "CA"))
        regions.append(Region(title: "Chile", image: "cl-60", smallerImage: "cl-30", code: "CL"))
        regions.append(Region(title: "Denmark", image: "dk-60", smallerImage: "dk-30", code: "DK"))
        regions.append(Region(title: "Finland", image: "fi-60", smallerImage: "fi-30", code: "FI"))
        regions.append(Region(title: "France", image: "fr-60", smallerImage: "fr-30", code: "FR"))
        regions.append(Region(title: "Germany", image: "de-60", smallerImage: "de-30", code: "DE"))
        regions.append(Region(title: "Hong Kong", image: "hk-60", smallerImage: "hk-30", code: "HK"))
        regions.append(Region(title: "India", image: "in-60", smallerImage: "in-30", code: "IN"))
        regions.append(Region(title: "Indonesia", image: "id-60", smallerImage: "id-30", code: "ID"))
        regions.append(Region(title: "Italy", image: "it-60", smallerImage: "it-30", code: "IT"))
        regions.append(Region(title: "Japan", image: "jp-60", smallerImage: "jp-30", code: "JP"))
        regions.append(Region(title: "Korea", image: "kr-60", smallerImage: "kr-30", code: "KR"))
        regions.append(Region(title: "Malaysia", image: "my-60", smallerImage: "my-30", code: "MY"))
        regions.append(Region(title: "Mexico", image: "mx-60", smallerImage: "mx-30", code: "MX"))
        regions.append(Region(title: "Netherlands", image: "nl-60", smallerImage: "nl-30", code: "NL"))
        regions.append(Region(title: "New Zealand", image: "nz-60", smallerImage: "nz-30", code: "NZ"))
        regions.append(Region(title: "Norway", image: "no-60", smallerImage: "no-30", code: "NO"))
        regions.append(Region(title: "People's Republic of China", image: "cn-60", smallerImage: "cn-30", code: "CN"))
        regions.append(Region(title: "Poland", image: "pl-60", smallerImage: "pl-30", code: "PL"))
        regions.append(Region(title: "Portugal", image: "pt-60", smallerImage: "pt-30", code: "PT"))
        regions.append(Region(title: "Republic of the Philippines", image: "ph-60", smallerImage: "ph-30", code: "PH"))
        regions.append(Region(title: "Russia", image: "ru-60", smallerImage: "ru-30", code: "RU"))
        regions.append(Region(title: "Saudi Arabia", image: "sa-60", smallerImage: "sa-30", code: "SA"))
        regions.append(Region(title: "South Africa", image: "za-60", smallerImage: "za-30", code: "ZA"))
        regions.append(Region(title: "Spain", image: "es-60", smallerImage: "es-30", code: "ES"))
        regions.append(Region(title: "Sweden", image: "se-60", smallerImage: "se-30", code: "SE"))
        regions.append(Region(title: "Switzerland", image: "ch-60", smallerImage: "ch-30", code: "CH"))
        regions.append(Region(title: "Taiwan", image: "tw-60", smallerImage: "tw-30", code: "TW"))
        regions.append(Region(title: "Turkey", image: "tr-60", smallerImage: "tr-30", code: "TR"))
        regions.append(Region(title: "United Kingdom", image: "gb-60", smallerImage: "gb-30", code: "GB"))
        regions.append(Region(title: "United States", image: "us-60", smallerImage: "us-30", code: "US"))
    }
    
}
