//
//  NewsProvider.swift
//  Covid-19
//
//  Created by Alwyn Yeo on 29/03/2020.
//  Copyright © 2020 Alwyn Dev. All rights reserved.
//

import RealmSwift
// MARK: - Struct
class NewsProvider: Object, Decodable {
    
    // MARK: - Declarations
    @objc private(set) dynamic var name: String?
}
