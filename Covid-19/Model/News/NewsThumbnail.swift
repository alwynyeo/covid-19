//
//  NewsThumbnail.swift
//  Covid-19
//
//  Created by Alwyn Yeo on 28/03/2020.
//  Copyright © 2020 Alwyn Dev. All rights reserved.
//

import RealmSwift
// MARK: - Struct
class NewsThumbnail: Object, Decodable {
    
    // MARK: - Declarations
    @objc private(set) dynamic var contentUrl: String?
}
