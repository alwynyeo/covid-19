//
//  NewsItem.swift
//  Covid-19
//
//  Created by Alwyn Yeo on 28/03/2020.
//  Copyright © 2020 Alwyn Dev. All rights reserved.
//

import RealmSwift

// MARK: - Struct
class NewsItem: Object, Decodable {
    
    // MARK: - Declarations
    @objc dynamic var id: String?
    @objc private(set) dynamic var name: String?
    @objc private(set) dynamic var url: String?
    @objc private(set) dynamic var image: NewsImage?
    @objc private(set) dynamic var datePublished: String?
    var provider = List<NewsProvider>()
    
    override class func primaryKey() -> String? {
        return "id"
    }
}
