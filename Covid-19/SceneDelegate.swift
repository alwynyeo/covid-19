//
//  SceneDelegate.swift
//  Covid-19
//
//  Created by Alwyn Yeo on 25/03/2020.
//  Copyright © 2020 Alwyn Dev. All rights reserved.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?
    private let locale = Locale.current
    private var hasAlreadyLaunched: Bool?
    private var isDarkMode: Bool?
    
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let windowScene = (scene as? UIWindowScene) else { return }
        
        let frame = windowScene.coordinateSpace.bounds
        window = UIWindow(frame: frame)
        window?.windowScene = windowScene
        window?.rootViewController = MainTabBarController()
        window?.makeKeyAndVisible()
        
        hasAlreadyLaunched = defaults.bool(forKey: globalHasAlreadyLaunchedKey)
                
        guard var hasAlreadyLaunched = hasAlreadyLaunched else { Errors.error(in: "SceneDelegate", at: #line); return }

        if hasAlreadyLaunched {
            hasAlreadyLaunched = true
        } else {
            configuration()
        }
    }
    
    private func configuration() {
        hasAlreadyLaunched = true
        defaults.set(hasAlreadyLaunched, forKey: globalHasAlreadyLaunchedKey)
        configureRegionCode()
        configureUserInterfaceStyle()
    }
    
    private func configureRegionCode() {
        if let code = locale.regionCode {
            defaults.set(code, forKey: globalRegionCodeKey)
        } else {
            defaults.set("US", forKey: globalRegionCodeKey)
        }
    }
    
    private func configureUserInterfaceStyle() {
        switch UITraitCollection.current.userInterfaceStyle {
            case .dark:
                isDarkMode = true
            case .light:
                isDarkMode = false
            case .unspecified:
                Errors.error(in: "SceneDelegate", at: #line)
            default:
                Errors.error(in: "SceneDelegate", at: #line)
        }
        defaults.setValue(isDarkMode, forKey: globalIsDarkModeKey)
    }
    
    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not neccessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }

    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
    }


}

