//
//  Alert.swift
//  Covid-19
//
//  Created by Alwyn Yeo on 03/04/2020.
//  Copyright © 2020 Alwyn Dev. All rights reserved.
//

import UIKit

// MARK: - Struct
struct Alert {
    static func okAlert(on viewController: UIViewController?, with title: String?, description message: String?, withCancel cancel: Bool) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        if cancel {
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            alertController.addAction(cancelAction)
        }
        alertController.addAction(okAction)
        DispatchQueue.main.async {
            viewController?.present(alertController, animated: true, completion: nil)
        }
    }
    
    static func tryAgainAlert(on viewController: UIViewController?, for view: UIView?, with title: String?, description message: String?, withCancel cancel: Bool) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let tryAgainAction = UIAlertAction(title: "Try again", style: .default) { (_) in
            view?.endEditing(false)
        }
        if cancel {
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            alertController.addAction(cancelAction)
        }
        alertController.addAction(tryAgainAction)
        DispatchQueue.main.async {
            viewController?.present(alertController, animated: true, completion: nil)
        }
    }
    
    static func dismissAlert(on viewController: UIViewController?, with title: String?) {
        let alertController = UIAlertController(title: title, message: nil, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Dismiss", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        DispatchQueue.main.async {
            viewController?.present(alertController, animated: true, completion: nil)
        }
    }
}
