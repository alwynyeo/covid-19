//
//  Functions.swift
//  Covid-19
//
//  Created by Alwyn Yeo on 28/03/2020.
//  Copyright © 2020 Alwyn Dev. All rights reserved.
//

import UIKit

func formattedString(data: Int) -> String {
    let numberFormatter = NumberFormatter()
    numberFormatter.numberStyle = .decimal
    guard let value = numberFormatter.string(from: NSNumber(value: data)) else { Errors.error(in: "DetailsViewController", at: #line); return "0" }
    return value
}

func checkUserInterfaceStyle(view: UIView) {
    view.backgroundColor = .systemBackground
}
