//
//  Date.swift
//  Covid-19
//
//  Created by Alwyn Yeo on 29/03/2020.
//  Copyright © 2020 Alwyn Dev. All rights reserved.
//

import Foundation

extension Date {
    
    static func getConvertedDate(dateString: String, format dateFormat: String) -> String? {
        let formatter = DateFormatter()
        let dateFormatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        formatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = dateFormat
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        if let convertedDate = formatter.date(from: dateString) {
            return dateFormatter.string(from: convertedDate)
        }
        return ""
    }
    
}
