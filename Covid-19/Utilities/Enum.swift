//
//  Enum.swift
//  Covid-19
//
//  Created by Alwyn Yeo on 09/11/2020.
//  Copyright © 2020 Alwyn Dev. All rights reserved.
//

//import Foundation

enum StatusCode: Int {
    case invalidRequest = 400 // 400 @ missing parameters
    case invalidAuthorization = 401 // 401 @ header is missing or the subscription key is invalid
    case insufficientAuthorization = 403 // 403 @ queries per month
    case httpNotAllowed = 410 // 410 @ use https instead of http
    case rateLimitExceeded = 429 // 429 @ queries per second
    case serverError = 500 // 500 @ internal server error
}

enum ViewController {
    case MainTabBarController
    case HomeViewController
    case NewsViewController
    case RegionViewController
}

enum Network: String {
    case success = "Internet Is Connected"
    case failed = "No Internet Connection"
    case unidentified = "Something went wrong"
}
