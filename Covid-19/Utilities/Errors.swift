//
//  Errors.swift
//  Covid-19
//
//  Created by Alwyn Yeo on 25/03/2020.
//  Copyright © 2020 Alwyn Dev. All rights reserved.
//

// MARK: - Struct
struct Errors {
    static func error(in file: String, at line: Int, error message: Error? = nil) {
        if file == "Extensions" {
            print("[Extensions - \(file): line \(line)]")
        } else {
            if let message = message {
                print("[Class - (\(file)) Error: \(message.localizedDescription as Any) at line \(line)]")
            } else {
                print("[Class - \(file): line \(line)]")
            }
        }
    }
}
