//
//  Networking.swift
//  Covid-19
//
//  Created by Alwyn Yeo on 25/03/2020.
//  Copyright © 2020 Alwyn Dev. All rights reserved.
//

import Alamofire

// MARK: - Class
class Networking {
    
    // MARK: - Declarations (Properties)
    private let covidApiHeaders: HTTPHeaders = [
        "x-rapidapi-host": "covid-193.p.rapidapi.com",
        "x-rapidapi-key": "408c2fb873msh00074171956edc3p1ad892jsn432e26088c53",
    ]
    private let newsApiHeaders: HTTPHeaders = [
        "Ocp-Apim-Subscription-Key": "500708ebfef84c3a91651d646a5ead77",
    ]
    private let covidApiUrl = "https://covid-193.p.rapidapi.com/statistics?"
    private let newsApiUrl = "https://api.cognitive.microsoft.com/bing/v7.0/news/search?"
    
    // MARK: - Singleton (Instance)
    static let instance = Networking()
    
    // MARK: - Methods (Functions)
    // Cases
    func getAllCovidResults(searchedText text: String, completion: @escaping ResultsHandler) {
        let params = ["country": text] // https://covid-193.p.rapidapi.com/statistics?country=All
        let encoding = URLEncoding.default // URLEncoding.default will turn spaces what it needs to be. For example, john+doe or john%20doe
        
        AF.request(covidApiUrl, method: .get, parameters: params, encoding: encoding, headers: covidApiHeaders).responseData { [weak self] response in
            
            guard let _ = self else { Errors.error(in: "Networking", at: #line, error: response.error); return }
        
            guard response.error == nil else {
                guard let error = response.error else {
                    Errors.error(in: "Networking", at: #line)
                    return
                }
                Errors.error(in: "Networking", at: #line, error: error)
                completion(.failure(error), nil)
                return
            }
            
            guard let data = response.data else {
                Errors.error(in: "Networking", at: #line, error: response.error)
                return
            }
            
            guard let statusCode = response.response?.statusCode else {
                Errors.error(in: "Networking", at: #line)
                return
            }
            
            do {
                // Decode the data(from response).
                // Decoded data is now the model.
                let covidResults = try JSONDecoder().decode(CaseResult.self, from: data)
                let results = covidResults.response
                completion(.success(results), statusCode)
                
            } catch let error {
                completion(.failure(error), statusCode)
                return
            }
        }
    }
    
    // News
    func getAllNewsResults(count: Int, offset: Int, completion: @escaping NewsResultsHandler) {
        let params: [String: Any] = [
            "q": "covid-19",
            "cc": UserDefaults.standard.string(forKey: globalRegionCodeKey) ?? "US",
            "count": count,
            "offset": offset,
        ]
        let encoding = URLEncoding.default
        
        AF.request(newsApiUrl, method: .get, parameters: params, encoding: encoding, headers: newsApiHeaders).responseData { [weak self] response in
            
            guard let _ = self else { Errors.error(in: "Networking", at: #line, error: response.error); return }
            
            guard response.error == nil else {
                guard let error = response.error else { Errors.error(in: "Networking", at: #line); return }
                completion(.failure(error), nil)
                Errors.error(in: "Networking", at: #line, error: error)
                return
            }
            
            guard let data = response.data else {
                Errors.error(in: "Networking", at: #line, error: response.error)
                return
            }
            
            guard let statusCode = response.response?.statusCode else {
                Errors.error(in: "Networking", at: #line)
                return
            }
            
            do {
                // Decode the data(from response).
                // Decoded data is now the model.
                let newsResults = try JSONDecoder().decode(NewsResult.self, from: data)
                let results = newsResults.value
                completion(.success(results), statusCode)
            } catch let error {
                completion(.failure(error), statusCode)
            }
        }
    }
    
}
