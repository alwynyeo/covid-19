//
//  NewsViewController.swift
//  Covid-19
//
//  Created by Alwyn Yeo on 25/03/2020.
//  Copyright © 2020 Alwyn Dev. All rights reserved.
//

import UIKit
import Network
import SafariServices
import RealmSwift

// MARK: - Protocol
protocol NewsViewControllerProtocol {
    func getRegionNewsResults(region code: String)
}
// MARK: - Class
class NewsViewController: UICollectionViewController {
    
    // MARK: - Declarations (Properties)
    private let reuseCellIdentifier = "cellID"
    private let reuseFooterIdentifier = "footerID"
    private var newsItems = [NewsItem]()
    private var isPaginating = false
    private var isDonePaginating = false
    private var isNetworkConnected = false
    private var isLastCell = false
    private var navigationBarHeight: CGFloat = 0
    private var isErrorOccurred = false
    private var realm: Realm?
    private var isAddedToRealm = false
    var delegate: NewsViewControllerProtocol?
    
    // MARK: - Declarations (UI)
    fileprivate lazy var statusBar = UIView()
    fileprivate let floatingView = FloatingView()
    fileprivate lazy var emptyStateLabel: UILabel = {
        let width = collectionView.bounds.width
        let height = collectionView.bounds.height
        let frame = CGRect(x: 0, y: 0, width: width, height: height)
        let label = UILabel(frame: frame)
        label.text = "No Data Available"
        label.font = .systemFont(ofSize: 30)
        label.textColor = .systemGray
        label.textAlignment = .center
        return label
    }()
    fileprivate lazy var rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "\(defaults.string(forKey: globalRegionCodeKey)?.lowercased() ?? "us")-30"), style: .plain, target: self, action: #selector(handleRightBarButtonItem(_:)))
    
    // MARK: - Overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        checkUserInterfaceStyle(view: collectionView)
        statusBar.backgroundColor = collectionView.backgroundColor
    }
    
    // MARK: - Methods (UI)
    private func setupView() {
        configureNavigationBar()
        configureCollectionView()
        configureRefreshControl()
        checkUserInterfaceStyle(view: collectionView)
        configureStatusBar()
        monitorNetworkChangesObserver()
        configureFloatingView()
        configureRealm()
        getAllNewsResults(count: 10)
    }
    
    private func configureNavigationBar() {
        navigationItem.title = "News"
        navigationController?.navigationBar.prefersLargeTitles = true
        extendedLayoutIncludesOpaqueBars = true
        navigationBarHeight = navigationController?.navigationBar.frame.height ?? 0
        navigationItem.rightBarButtonItem = rightBarButtonItem
    }
    
    private func configureCollectionView() {
        collectionView.alwaysBounceVertical = true
        collectionView.contentInsetAdjustmentBehavior = .always
        registerCell()
    }
    
    private func registerCell() {
        collectionView.register(NewsCell.self, forCellWithReuseIdentifier: reuseCellIdentifier)
        collectionView.register(NewsFooterView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: reuseFooterIdentifier)
    }
    
    private func configureStatusBar() {
        guard let windowScene = navigationController?.view.window?.windowScene else { Errors.error(in: "\(ViewController.NewsViewController)", at: #line); return }
        guard let frame = windowScene.statusBarManager?.statusBarFrame else { Errors.error(in: "\(ViewController.NewsViewController)", at: #line); return }
        statusBar.frame = frame
        statusBar.alpha = 1
        statusBar.backgroundColor = collectionView.backgroundColor
        navigationController?.view.addSubview(statusBar)
    }
    
    private func configureRefreshControl() {
        collectionView.refreshControl = UIRefreshControl()
        collectionView.refreshControl?.layer.zPosition = navigationController?.navigationBar.layer.zPosition ?? 1 + 1
        collectionView.refreshControl?.addTarget(self, action: #selector(handleRefreshControl(_:)), for: .valueChanged)
    }
    
    private func configureFloatingView() {
        collectionView.addSubview(floatingView)
        NSLayoutConstraint.activate([
            floatingView.leadingAnchor.constraint(equalTo: collectionView.safeAreaLayoutGuide.leadingAnchor, constant: 16),
            floatingView.trailingAnchor.constraint(equalTo: collectionView.safeAreaLayoutGuide.trailingAnchor, constant: -16),
            floatingView.bottomAnchor.constraint(equalTo: collectionView.safeAreaLayoutGuide.bottomAnchor, constant: 90),
            floatingView.heightAnchor.constraint(equalToConstant: 50),
        ])
    }
    
    // MARK: - Methods (Functions)
    private func getAllNewsResults(count: Int = 10, offset: Int = 0, isRefreshing: Bool = false) {
        Networking.instance.getAllNewsResults(count: count, offset: offset) { [weak self] status, code in
            
            guard let self = self else { Errors.error(in: "\(ViewController.NewsViewController)", at: #line); return }
            
            switch status {
                case .success(let items):
                    self.success(items: items, code: code, isRefreshing: isRefreshing)
                case .failure(let error):
                    self.failure(error: error, code: code)
            }
        }
    }
    
    private func success(items: [NewsItem]?, code: Int?, isRefreshing: Bool) {
        guard let items = items else {
            print("(Success) - Status code: \(code as Any)")
            checkStatusCode(statusCode: code)
            showOfflineData()
            Errors.error(in: "\(ViewController.NewsViewController)", at: #line)
            return
        }
        
        if isRefreshing {
            isAddedToRealm = false
            self.newsItems.removeAll()
            DispatchQueue.main.async {
                self.collectionView.refreshControl?.endRefreshing()
            }
        }

        newsItems.append(contentsOf: items)
        // Save the data to Realm
        addToRealm(items: items)
        
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
        
        if items.count == 0 {
            isDonePaginating = true
        }
    }
    
    private func failure(error: Error?, code: Int?) {
        print("(Failure) - Status code: \(code as Any)")
        checkStatusCode(statusCode: code)
        showOfflineData()
        Errors.error(in: "\(ViewController.NewsViewController)", at: #line, error: error)
    }
    
    private func animateFloatingView() {
        if floatingView.transform == .identity {
            let y: CGFloat = -90 - (navigationBarHeight - 36)
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                UIView.animate(withDuration: 0.7, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.7, options: .curveEaseOut) {
                    self.floatingView.transform = .init(translationX: 0, y: y)
                } completion: { [weak self] (success) in
                    guard let self = self else {
                        Errors.error(in: "\(ViewController.NewsViewController)", at: #line)
                        return
                    }
                    guard success == true else {
                        Errors.error(in: "\(ViewController.NewsViewController)", at: #line)
                        return
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                        UIView.animate(withDuration: 0.7, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.7, options: .curveEaseOut) {
                            self.floatingView.transform = .identity
                        }
                    }
                }
            }
        }
    }
    
    private func checkStatusCode(statusCode code: Int?) {
        guard code != nil else {
            isErrorOccurred = true
            floatingView.configure(network: .failed)
            animateFloatingView()
            Errors.error(in: "\(ViewController.NewsViewController)", at: #line)
            return
        }
        
        if code == 200 {
            isErrorOccurred = false
            floatingView.configure(network: .success)
            if collectionView.backgroundView != nil {
                collectionView.backgroundView = nil
            }
        } else {
            isErrorOccurred = true
            floatingView.configure(network: .unidentified)
            animateFloatingView()
        }
    }
    
    private func showOfflineData() {
        guard let realmObjects = realm?.objects(NewsItem.self) else {
            Errors.error(in: "\(ViewController.NewsViewController)", at: #line)
            return
        }
        // Check if realm has data
        if realmObjects.count > .zero {
            if newsItems.isEmpty {
                self.getFromRealm()
            }
        } else {
            // Show empty state label
            DispatchQueue.main.async {
                if self.collectionView.backgroundView == nil {
                    self.collectionView.backgroundView = self.emptyStateLabel
                }
            }
        }
        DispatchQueue.main.async {
            self.collectionView.reloadData()
            self.collectionView.refreshControl?.endRefreshing()
        }
    }
    
    private func addToRealm(items: [NewsItem]) {
        // Check if the data has added to Realm
        if !isAddedToRealm {
            var id = 0
            let offlineData = items.prefix(10)
            offlineData.forEach { (item) in
                id += 1
                item.id = "\(id)"
            }
            do {
                try realm?.write({ [weak self] in
                    guard let _ = self else {
                        Errors.error(in: "\(ViewController.NewsViewController)", at: #line)
                        return
                    }
                    realm?.add(offlineData, update: .modified)
                })
            } catch let error {
                Errors.error(in: "\(ViewController.NewsViewController)", at: #line, error: error)
            }
            isAddedToRealm = true
            print("Add to Realm")
        }
    }
    
    private func getFromRealm() {
        guard let realmObjects = realm?.objects(NewsItem.self) else {
            Errors.error(in: "\(ViewController.NewsViewController)", at: #line)
            return
        }
        DispatchQueue.main.async {
            self.newsItems.append(contentsOf: realmObjects)
            print("Get from Realm")
        }
    }
    
    private func configureRealm() {
        do {
            realm = try Realm()
        } catch let error {
            Errors.error(in: "\(ViewController.NewsViewController)", at: #line, error: error)
        }
        print(Realm.Configuration.defaultConfiguration.fileURL as Any)
    }
    
    private func retrieveNewRegionNewsResults(region code: String) {
        rightBarButtonItem.image = UIImage(named: "\(defaults.string(forKey: globalRegionCodeKey)?.lowercased() ?? "us")-30")
        getAllNewsResults(isRefreshing: true)
    }
    
    private func monitorNetworkChangesObserver() {
        let wifiMonitor = NWPathMonitor(requiredInterfaceType: .wifi)
        let cellularMonitor = NWPathMonitor(requiredInterfaceType: .cellular)
        let queue = DispatchQueue.global(qos: .background)
        
        wifiMonitor.pathUpdateHandler = { [weak self] (path) in
            
            guard let self = self else { Errors.error(in: "\(ViewController.NewsViewController)", at: #line); return }
            let status = path.status
            
            if status == .satisfied {
                DispatchQueue.main.async {
                    self.isNetworkConnected = true
                    self.navigationItem.rightBarButtonItem?.isEnabled = true
                    if self.isLastCell {
                        print("call news by wifi")
                    }
                }
            } else if status == .unsatisfied {
                DispatchQueue.main.async {
                    self.isNetworkConnected = false
                    self.navigationItem.rightBarButtonItem?.isEnabled = false
                    Errors.error(in: "\(ViewController.NewsViewController)", at: #line)
                }
            } else {
                DispatchQueue.main.async {
                    self.isNetworkConnected = false
                    self.navigationItem.rightBarButtonItem?.isEnabled = false
                    Alert.okAlert(on: self, with: "Requires connection wifi", description: nil, withCancel: false)
                }
            }
            
        }
        
        cellularMonitor.pathUpdateHandler = { [weak self] (path) in
            
            guard let self = self else { Errors.error(in: "\(ViewController.NewsViewController)", at: #line); return }
            let status = path.status
            
            if status == .satisfied {
                if wifiMonitor.currentPath.status == .unsatisfied {
                    DispatchQueue.main.async {
                        self.isNetworkConnected = true
                        self.navigationItem.rightBarButtonItem?.isEnabled = true
                        if self.isLastCell {
                            print("call news by cellular")
                        }
                    }
                }
            } else if status == .unsatisfied {
                DispatchQueue.main.async {
                    if wifiMonitor.currentPath.status == .unsatisfied {
                        self.isNetworkConnected = false
                        self.navigationItem.rightBarButtonItem?.isEnabled = false
                        Errors.error(in: "\(ViewController.NewsViewController)", at: #line)
                    }
                }
            } else {
                DispatchQueue.main.async {
                    if wifiMonitor.currentPath.status == .requiresConnection {
                        self.isNetworkConnected = false
                        self.navigationItem.rightBarButtonItem?.isEnabled = false
                        Alert.okAlert(on: self, with: "Requires connection cellular", description: nil, withCancel: false)
                    }
                }
            }
            
        }
        // Start monitoring.
        wifiMonitor.start(queue: queue)
        cellularMonitor.start(queue: queue)
    }
    
    // MARK: - Objc Methods
    @objc private func handleRightBarButtonItem(_ sender: UIBarButtonItem?) {
        let regionViewController = RegionViewController()
        regionViewController.delegate = self
        let navigationController = UINavigationController(rootViewController: regionViewController)
        present(navigationController, animated: true, completion: nil)
    }
    
    @objc private func handleRefreshControl(_ sender: UIRefreshControl?) {
        getAllNewsResults(isRefreshing: true)
    }
    
    // MARK: - Deinit
    deinit {
        
    }
    
}

// MARK: - Extension
extension NewsViewController {
    // Header & Footer
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        guard let footer = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: reuseFooterIdentifier, for: indexPath) as? NewsFooterView else {
            return UICollectionReusableView()
        }
        return footer
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        let height: CGFloat = isDonePaginating || isErrorOccurred ? .zero : 150
        return .init(width: collectionView.frame.width, height: height)
    }
    
    // Cell
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseCellIdentifier, for: indexPath) as? NewsCell else { Errors.error(in: "\(ViewController.NewsViewController)", at: #line); return UICollectionViewCell()
        }
        
        let newsItem = newsItems[indexPath.item]
        cell.configure(newsItems: newsItem)
        
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let newsItem = newsItems[indexPath.item]
        guard let newsString = newsItem.url else { Errors.error(in: "\(ViewController.NewsViewController)", at: #line); return }
        guard let newsUrl = URL(string: newsString) else { Errors.error(in: "\(ViewController.NewsViewController)", at: #line)
            return
        }
        let safariViewController = SFSafariViewController(url: newsUrl)
        
        DispatchQueue.main.async {
            self.present(safariViewController, animated: true, completion: nil)
        }
    }

    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        // Pagination
        // If it's the last cell or the last cell is visible, perform pagination.
        if indexPath.item == newsItems.count - 1 || collectionView.indexPathsForVisibleItems.contains([0, newsItems.count - 1]) {
            isLastCell = true
            if !isPaginating && !isDonePaginating && isNetworkConnected {
                isPaginating = true
                getAllNewsResults(offset: newsItems.count)
                isLastCell = false
                isPaginating = false
            }
        } else {
            if isLastCell {
                isLastCell = false
            }
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return newsItems.count
    }
    
    // Horizontal Line
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

// Delegate Flow Layout
extension NewsViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 120)
    }
}

// Delegate
extension NewsViewController: RegionViewControllerDelegate {
    func getRegionNewsResults(region code: String) {
        retrieveNewRegionNewsResults(region: code)
    }
}

// Delegate
extension NewsViewController: NewsHeaderViewProtocol {
    func didPressCountryButton() {
        let regionViewController = RegionViewController()
        regionViewController.delegate = self
        let navigationController = UINavigationController(rootViewController: regionViewController)
        present(navigationController, animated: true, completion: nil)
    }
}

extension NewsViewControllerProtocol {
    func getRegionNewsResults(region code: String) {}
}
