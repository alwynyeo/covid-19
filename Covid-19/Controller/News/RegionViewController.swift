//
//  RegionViewController.swift
//  Covid-19
//
//  Created by Alwyn Yeo on 30/06/2020.
//  Copyright © 2020 Alwyn Dev. All rights reserved.
//

import UIKit

// MARK: - Protocol
protocol RegionViewControllerDelegate {
    func getRegionNewsResults(region code: String)
}

// MARK: - Class
class RegionViewController: UITableViewController {
    
    // MARK: - Declarations (Properties)
    private let reuseCellIdentifier = "cellID"
    private let regions = Regions().regions
    private var regionCode: String?
    
    var delegate: RegionViewControllerDelegate?
    
    // MARK: - Declarations (UI)
    fileprivate lazy var leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self,
                                                             action: #selector(handleBarButtonItems(_:)))
    
    fileprivate lazy var rightBarButtonItem: UIBarButtonItem = {
        let item = UIBarButtonItem(title: "Done", style: .done, target: self,
                                   action: #selector(handleBarButtonItems(_:)))
        item.isEnabled = false
        item.tintColor = .clear
        return item
    }()
    
    // MARK: - Overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        checkUserInterfaceStyle(view: tableView)
    }
    
    // MARK: - Methods (UI)
    private func setupView() {
        navigationItem.title = "Region"
        navigationItem.leftBarButtonItem = leftBarButtonItem
        navigationItem.rightBarButtonItem = rightBarButtonItem
        configureTableView()
        checkUserInterfaceStyle(view: tableView)
    }
    
    private func configureTableView() {
        tableView.tableFooterView = UIView()
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        registerCell()
    }
    
    private func registerCell() {
        tableView.register(RegionCell.self, forCellReuseIdentifier: reuseCellIdentifier)
    }
    
    // MARK: - Methods (Functions)
    private func setBarButtonStates(isEnabled bool: Bool, tintColor color: UIColor) {
        rightBarButtonItem.isEnabled = bool
        rightBarButtonItem.tintColor = color
    }
    
    // MARK: - Objc Methods
    @objc private func handleBarButtonItems(_ sender: UIBarButtonItem?) {
        guard let sender = sender else { Errors.error(in: "\(ViewController.RegionViewController)", at: #line); return }
        
        if sender.title == "Done" {
            defaults.set(regionCode, forKey: globalRegionCodeKey)
            if let regionCode = self.regionCode {
                delegate?.getRegionNewsResults(region: regionCode.lowercased())
            } else {
                delegate?.getRegionNewsResults(region: "us")
            }
        }
        
        dismiss(animated: true, completion: nil)
        setBarButtonStates(isEnabled: false, tintColor: .clear)
    }
    
    // MARK: - Deinit
    deinit {
        
    }
    
}

// MARK: - Extension
extension RegionViewController {
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: reuseCellIdentifier, for: indexPath) as? RegionCell else { Errors.error(in: "\(ViewController.RegionViewController)", at: #line, error: nil); return UITableViewCell() }
        cell.setRegion(region: regions[indexPath.item])
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        setBarButtonStates(isEnabled: true, tintColor: #colorLiteral(red: 0.006537661422, green: 0.4778559804, blue: 0.9984870553, alpha: 1))
        regionCode = regions[indexPath.item].code
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        // #warning Incomplete implementation, return the height of each cell
        return 80
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return regions.count
    }
}

extension RegionViewControllerDelegate {
    func getRegionNewsResults(region code: String) {}
}
