//
//  MainTabBarController.swift
//  Covid-19
//
//  Created by Alwyn Yeo on 25/03/2020.
//  Copyright © 2020 Alwyn Dev. All rights reserved.
//

import UIKit

// MARK: - Class
class MainTabBarController: UITabBarController {
    
    // MARK: - Declarations (Properties)
    
    // MARK: - Declarations (UI)
    
    // MARK: - Overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    // MARK: - Methods (UI)
    private func setupView() {
        view.backgroundColor = .clear
        setupViewControllers()
    }
    
    private func setupViewControllers() {
        let homeNavigationController = generateNavigationController(unselectedImage: UIImage(systemName: "house"), selectedImage: UIImage(systemName: "house.fill"), viewController: HomeViewController())
        
        let newsViewController = NewsViewController(collectionViewLayout: globalNewsViewControllerLayout)
        let newsNavigationController = generateNavigationController(unselectedImage: UIImage(systemName: "globe"), selectedImage: UIImage(systemName: "globe"), viewController: newsViewController)
        
        viewControllers = [homeNavigationController, newsNavigationController]
        
        guard let items = tabBar.items else { Errors.error(in: "\(ViewController.MainTabBarController)", at: #line, error: nil); return }
        
        for item in items {
            item.imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)
        }
    }
    
    // MARK: - Methods (Functions)
    private func generateNavigationController(unselectedImage: UIImage?, selectedImage: UIImage?, viewController controller: UIViewController = UIViewController(), isTranslucent translucent: Bool = false) -> UINavigationController {
        
        let navigationController = UINavigationController(rootViewController: controller)
        
        navigationController.tabBarItem.image = unselectedImage
        navigationController.tabBarItem.selectedImage = selectedImage
        navigationController.tabBarItem.title = nil
        navigationController.navigationBar.isTranslucent = translucent
        tabBar.isTranslucent = translucent
        
        return navigationController
    }
    
    // MARK: - Objc Methods
    
    // MARK: - Deinit
    deinit {
        
    }
    
}
