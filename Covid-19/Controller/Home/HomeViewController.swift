//
//  HomeViewController.swift
//  Covid-19
//
//  Created by Alwyn Yeo on 25/03/2020.
//  Copyright © 2020 Alwyn Dev. All rights reserved.
//

import UIKit
import Network
import RealmSwift
import Charts

// MARK: - Protocol
protocol HomeViewControllerProtocol {
    func getResponseMessage(network: Network)
}

// MARK: - Class
class HomeViewController: UIViewController {
    
    // MARK: - Declarations (Properties)
    private var country: String?
    private var total = "Total"
    private var new = "New"
    private var active = "Active"
    private var critical = "Critical"
    private var recovered = "Recovered"
    private var newDeaths = "New Deaths"
    private var totalDeaths = "Deaths"
    private var caseDictionary = [String: Int]()
    var delegate: HomeViewControllerProtocol?
    
    // MARK: - Declarations (UI)
    fileprivate let moonIconLight = UIImage(systemName: "moon")?.withRenderingMode(.alwaysOriginal)
    
    fileprivate let moonIconDark = UIImage(systemName: "moon.fill")?.withRenderingMode(.alwaysOriginal)
    
    fileprivate let refreshIconLight = UIImage(systemName: "arrow.clockwise")?.withRenderingMode(.alwaysOriginal)
    
    fileprivate let refreshIconDark = UIImage(systemName: "arrow.clockwise")?.withTintColor(.white, renderingMode: .alwaysOriginal)
    
    fileprivate lazy var leftBarButtonItem: UIBarButtonItem = {
        let item = UIBarButtonItem(image: moonIconLight, style: .plain, target: self, action: #selector(handleBarButtonItems(_:)))
        item.tag = 0
        return item
    }()
    
    fileprivate lazy var rightBarButtonItem: UIBarButtonItem = {
        let item = UIBarButtonItem(image: refreshIconLight, style: .plain, target: self, action: #selector(handleBarButtonItems(_:)))
        item.tag = 1
        return item
    }()
    
    fileprivate let rightBarbuttonActivityIndicatorView = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
    fileprivate lazy var rightBarButtonActivityIndicator = UIBarButtonItem(customView: rightBarbuttonActivityIndicatorView)
    
    fileprivate lazy var frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
    fileprivate lazy var cardView = CardView(frame: frame)
    fileprivate lazy var chartView = ChartView(frame: frame)
    
    fileprivate let activityIndicatorView = ActivityIndicatorView(style: .medium, isAnimating: true)
    
    fileprivate var totalCasesDataChart = PieChartDataEntry(value: 0)
    fileprivate var recoveredCasesDataChart = PieChartDataEntry(value: 0)
    fileprivate var totalDeathsDataChart = PieChartDataEntry(value: 0)
    
    fileprivate lazy var segmentedControl: UISegmentedControl = {
        let items = ["Card", "Chart"]
        let segmentedControl = UISegmentedControl(items: items)
        for index in 0...items.count - 1 {
            segmentedControl.setWidth(90, forSegmentAt: index)
        }
        segmentedControl.selectedSegmentIndex = 0
        segmentedControl.addTarget(self, action: #selector(handleSegmentedControl(_:)), for: .valueChanged)
        return segmentedControl
    }()
    
    fileprivate lazy var countryTextField: CountryTextfield = {
        let textfield = CountryTextfield()
        textfield.setDefaultText(default: "Global")
        textfield.inputView = countryPickerView
        textfield.inputAccessoryView = countryPickerViewToolBar
        return textfield
    }()
    
    fileprivate lazy var countryPickerView: UIPickerView = {
        let view = UIPickerView()
        view.delegate = self
        return view
    }()
    
    fileprivate lazy var countryPickerViewToolBar: PickerViewToolBar = {
        let frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 49)
        let toolBar = PickerViewToolBar(frame: frame)
        toolBar.cancelBarButtonItem.action = #selector(handleToolBarButtonItem(_:))
        toolBar.doneBarButtonItem.action = #selector(handleToolBarButtonItem(_:))
        return toolBar
    }()
    
    // MARK: - Overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()

    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        if !defaults.bool(forKey: globalIsWindowUserInterfaceStyleOverridenKey) {
            updateDarkModeKey()
            configureUserInterfaceStyle(userInterfaceStyle: traitCollection.userInterfaceStyle)
        }
    }
    
    // MARK: - Methods (UI)
    private func setupView() {
        navigationItem.titleView = segmentedControl
        navigationItem.leftBarButtonItem = leftBarButtonItem
        setRightBarButtonItem(isLoading: true)
        configureCountryTextField()
        configureCardView()
        configureChartView()
        checkUserInterfaceStyle()
        monitorNetworkChangesObserver()
        getCovidResults(country: getCountryText(text: "Global"))
    }
    
    private func configureCountryTextField() {
        view.addSubview(countryTextField)
        NSLayoutConstraint.activate([
            countryTextField.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 20),
            countryTextField.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor,
                                                      constant: 80),
            countryTextField.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor,
                                                       constant: -80),
            countryTextField.heightAnchor.constraint(equalToConstant: 34),
        ])
    }
    
    private func configureCardView() {
        cardView.homeViewController = self
        view.addSubview(cardView)
        NSLayoutConstraint.activate([
            cardView.topAnchor.constraint(equalTo: countryTextField.safeAreaLayoutGuide.bottomAnchor, constant: 20),
            cardView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            cardView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
            cardView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
        ])
    }
    
    private func configureChartView() {
        view.addSubview(chartView)
        NSLayoutConstraint.activate([
            chartView.topAnchor.constraint(equalTo: countryTextField.safeAreaLayoutGuide.bottomAnchor, constant: 20),
            chartView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            chartView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
            chartView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
        ])
    }
    
    private func setRightBarButtonItem(isLoading: Bool = false) {
        if isLoading {
            rightBarbuttonActivityIndicatorView.startAnimating()
            navigationItem.setRightBarButton(rightBarButtonActivityIndicator, animated: true)
        } else {
            rightBarbuttonActivityIndicatorView.stopAnimating()
            navigationItem.setRightBarButton(rightBarButtonItem, animated: true)
        }
        view.layoutIfNeeded()
    }
    
    // MARK: - Methods (Functions)
    private func getCovidResults(country text: String) {
        Networking.instance.getAllCovidResults(searchedText: text) { [weak self] status, code  in
            
            guard let self = self else { Errors.error(in: "\(ViewController.HomeViewController)", at: #line); return }
            
            switch status {
                case .success(let items):
                    self.success(items: items, code: code)
                case .failure(let error):
                    self.failure(error: error, code: code)
            }
            self.setRightBarButtonItem(isLoading: false)
        }
    }
    
    private func success(items: [CaseItem]?, code: Int?) {
        guard let items = items else {
            print("(Success) - Status code: \(code as Any)")
            checkStatusCode(statusCode: code)
            showEmptyState()
            Errors.error(in: "\(ViewController.HomeViewController)", at: #line)
            return
        }
        
        items.forEach { [weak self] (data) in
            
            guard let self = self else { Errors.error(in: "\(ViewController.HomeViewController)", at: #line); return }
            guard let cases = data.cases else { Errors.error(in: "\(ViewController.HomeViewController)", at: #line); return }
            guard let deaths = data.deaths else { Errors.error(in: "\(ViewController.HomeViewController)", at: #line); return }
            let totalCases = cases.total ?? 0
            let newCases = Int(cases.new ?? "0") ?? 0
            let activeCases = cases.active ?? 0
            let criticalCases = cases.critical ?? 0
            let recoveredCases = cases.recovered ?? 0
            let newDeathCases = Int(deaths.new ?? "0") ?? 0
            let totalDeathCases = deaths.total ?? 0
            
            if let country = data.country {
                if country == "All" {
                    self.country = "Global"
                }
            }
            
            self.caseDictionary = [
                total: totalCases,
                new: newCases,
                active: activeCases,
                critical: criticalCases,
                recovered: recoveredCases,
                newDeaths: newDeathCases,
                totalDeaths: totalDeathCases,
            ]
            
        }
        
        cardView.configure(
            total: caseDictionary[total] ?? 0,
            new: caseDictionary[new] ?? 0,
            active: caseDictionary[active] ?? 0,
            critical: caseDictionary[critical] ?? 0,
            recovered: caseDictionary[recovered] ?? 0,
            newDeaths: caseDictionary[newDeaths] ?? 0,
            deaths: caseDictionary[totalDeaths] ?? 0
        )
        
        chartView.setPieChartDataEntry(
            total: Double(caseDictionary[total] ?? 0),
            deaths: Double(caseDictionary[totalDeaths] ?? 0),
            recovered: Double(caseDictionary[recovered] ?? 0)
        )
        
        delegate?.getResponseMessage(network: .success)
        view.endEditing(true)
    }
    
    private func failure(error: Error?, code: Int?) {
        DispatchQueue.main.async {
            print("(Success) - Status code: \(code as Any)")
            self.checkStatusCode(statusCode: code)
            self.showEmptyState()
        }
        Errors.error(in: "\(ViewController.HomeViewController)", at: #line, error: error)
    }
    
    private func checkStatusCode(statusCode code: Int?) {
        guard code != nil else {
            delegate?.getResponseMessage(network: .failed)
            Errors.error(in: "\(ViewController.NewsViewController)", at: #line)
            return
        }
        
        if code == 200 {
            delegate?.getResponseMessage(network: .success)
        } else {
            delegate?.getResponseMessage(network: .unidentified)
        }
    }
    
    private func showEmptyState() {
        DispatchQueue.main.async {
            self.cardView.configure(
                total: 0,
                new: 0,
                active: 0,
                critical: 0,
                recovered: 0,
                newDeaths: 0,
                deaths: 0
            )
            self.activityIndicatorView.stopAnimating()
        }
    }
    
    private func enableDefaultView(isEnabled bool: Bool) {
        if bool {
            chartView.isHidden = true
            cardView.isHidden = false
        } else {
            cardView.isHidden = true
            chartView.isHidden = false
        }
    }
    
    private func refreshData() {
        setRightBarButtonItem(isLoading: true)
        guard let text = countryTextField.text else {
            Errors.error(in: "\(ViewController.HomeViewController)", at: #line)
            return
        }
        getCovidResults(country: getCountryText(text: text))
    }
    
    private func cancel() {
        countryTextField.text = country
        view.endEditing(true)
    }
    
    private func getCountryText(text: String) -> String {
        if text == "Global" {
            return "All"
        }
        return text
    }
    
    private func checkUserInterfaceStyle() {
        if defaults.bool(forKey: globalIsWindowUserInterfaceStyleOverridenKey) {
            if defaults.bool(forKey: globalIsDarkModeKey) {
                configureUserInterfaceStyle(userInterfaceStyle: .dark, overrideUserInterfaceStyle: true)
            } else {
                configureUserInterfaceStyle(userInterfaceStyle: .light, overrideUserInterfaceStyle: true)
            }
        } else {
            if defaults.bool(forKey: globalIsDarkModeKey) {
                configureUserInterfaceStyle(userInterfaceStyle: .dark)
            } else {
                configureUserInterfaceStyle(userInterfaceStyle: .light)
            }
        }
    }
    
    private func configureUserInterfaceStyle(userInterfaceStyle style: UIUserInterfaceStyle, overrideUserInterfaceStyle: Bool = false) {
        updateWindowUserInterfaceStyle(userInterfaceStyle: style, overrideUserInterfaceStyle: overrideUserInterfaceStyle)
        switch style {
            case .dark:
                updateUserInterfaceStyle(leftItemImage: moonIconDark, rightItemImage: refreshIconDark, tabBarColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0))
            case .light:
                updateUserInterfaceStyle(leftItemImage: moonIconLight, rightItemImage: refreshIconLight, tabBarColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
            case .unspecified:
                Errors.error(in: "\(ViewController.HomeViewController)", at: #line)
            default:
                Errors.error(in: "\(ViewController.HomeViewController)", at: #line)
        }
    }
    
    private func updateWindowUserInterfaceStyle(userInterfaceStyle style: UIUserInterfaceStyle, overrideUserInterfaceStyle: Bool = false) {
        if overrideUserInterfaceStyle {
            UIApplication.shared.windows.forEach { [weak self] (window) in
                guard let _ = self else { Errors.error(in: "\(ViewController.HomeViewController)", at: #line); return }
                window.overrideUserInterfaceStyle = style
            }
        }
    }
    
    private func updateUserInterfaceStyle(leftItemImage: UIImage?, rightItemImage: UIImage?, tabBarColor: UIColor) {
        leftBarButtonItem.image = leftItemImage
        rightBarButtonItem.image = rightItemImage
        view.backgroundColor = .systemBackground
        tabBarController?.tabBar.tintColor = tabBarColor
        tabBarController?.tabBar.unselectedItemTintColor = tabBarColor
        countryTextField.layer.borderColor = UIColor.systemGray.cgColor
    }
    
    private func updateUI() {
        if defaults.bool(forKey: globalIsDarkModeKey) {
            configureUserInterfaceStyle(userInterfaceStyle: .light, overrideUserInterfaceStyle: true)
        } else {
            configureUserInterfaceStyle(userInterfaceStyle: .dark, overrideUserInterfaceStyle: true)
        }
        updateDarkModeKey()
        updateWindowUserInterfaceStyleOverridenKey()
    }
    
    private func updateDarkModeKey() {
        defaults.setValue(!defaults.bool(forKey: globalIsDarkModeKey), forKey: globalIsDarkModeKey)
    }
    
    private func updateWindowUserInterfaceStyleOverridenKey() {
        if !defaults.bool(forKey: globalIsWindowUserInterfaceStyleOverridenKey) {
            defaults.setValue(!defaults.bool(forKey: globalIsWindowUserInterfaceStyleOverridenKey), forKey: globalIsWindowUserInterfaceStyleOverridenKey)
        }
    }
    
    private func monitorNetworkChangesObserver() {
        let monitor = NWPathMonitor()
        
        monitor.pathUpdateHandler = { [weak self] (path) in
            
            guard let self = self else {
                Errors.error(in: "HomeViewController", at: #line)
                return
            }
            let status = path.status
            
            if status == .satisfied {
                DispatchQueue.main.async {
                    self.delegate?.getResponseMessage(network: .success)
                    self.refreshData()
                }
            } else if status == .unsatisfied {
                DispatchQueue.main.async {
                    self.delegate?.getResponseMessage(network: .failed)
                }
            } else {
                DispatchQueue.main.async {
                    Alert.okAlert(on: self, with: "Requires connection", description: nil, withCancel: false)
                }
            }
            
        }
        
        monitor.start(queue: .global())
    }
    
    // MARK: - Objc Methods
    @objc private func handleBarButtonItems(_ sender: UIBarButtonItem?) {
        guard let sender = sender else { Errors.error(in: "\(ViewController.HomeViewController)", at: #line); return }
        let tag = sender.tag
        
        if tag == 0 {
            updateUI()
        } else if tag == 1 {
            refreshData()
        }
    }
    
    @objc private func handleSegmentedControl(_ sender: UISegmentedControl?) {
        guard let sender = sender else { Errors.error(in: "\(ViewController.HomeViewController)", at: #line); return }
        
        switch sender.selectedSegmentIndex {
            case 0:
                enableDefaultView(isEnabled: true)
            case 1:
                enableDefaultView(isEnabled: false)
            default:
                print("Default")
        }
    }
    
    @objc private func handleToolBarButtonItem(_ sender: UIBarButtonItem?) {
        guard let sender = sender else { Errors.error(in: "\(ViewController.HomeViewController)", at: #line); return }
        let title = sender.title
        
        switch title {
            case "Done":
                refreshData()
            case "Cancel":
                cancel()
            default:
                print("Default")
        }
    }
    
    // MARK: - Deinit
    deinit {
        
    }
    
}

// MARK - Extension
extension HomeViewController: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return globalCountries[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        countryTextField.text = globalCountries[row]
    }
}

extension HomeViewController: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return globalCountries.count
    }
}

extension HomeViewControllerProtocol {
    func getResponseMessage(network: Network) {}
}
