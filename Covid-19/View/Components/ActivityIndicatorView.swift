//
//  ActivityIndicatorView.swift
//  Covid-19
//
//  Created by Alwyn Yeo on 03/04/2020.
//  Copyright © 2020 Alwyn Dev. All rights reserved.
//

import UIKit

class ActivityIndicatorView: UIActivityIndicatorView {
    
    // MARK: - Declarations(Properties)
    
    // MARK: - Overrides
    init(style: UIActivityIndicatorView.Style, isAnimating bool: Bool) {
        super.init(style: style)
        setupView(isAnimating: bool)
    }
    
    // MARK: - Methods (Functions)
    private func setupView(isAnimating bool: Bool) {
        hidesWhenStopped = true
        translatesAutoresizingMaskIntoConstraints = false
        if bool {
            startAnimating()
        } else {
            stopAnimating()
        }
    }
    
    // MARK: - //
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
