//
//  PickerViewToolBar.swift
//  Covid-19
//
//  Created by Alwyn Yeo on 04/04/2020.
//  Copyright © 2020 Alwyn Dev. All rights reserved.
//

import UIKit

class PickerViewToolBar: UIToolbar {
    
    // MARK: - Declarations (Properties)
    
    // MARK: - Declarations (UI)
    fileprivate let flexibleBarButtonItem = UIBarButtonItem(
        barButtonSystemItem: .flexibleSpace,
        target: nil,
        action: nil
    )
    
    let cancelBarButtonItem: UIBarButtonItem = {
        let barButtonItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: nil)
        return barButtonItem
    }()
    
    let doneBarButtonItem: UIBarButtonItem = {
        let barButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: nil)
        return barButtonItem
    }()
    
    // MARK: - Overrides
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    // MARK: - Methods (UI)
    private func setupView() {
        sizeToFit()
        setItems([
            cancelBarButtonItem,
            flexibleBarButtonItem,
            doneBarButtonItem,
        ], animated: true)
        translatesAutoresizingMaskIntoConstraints = false
    }
    
    // MARK: - Methods (Functions)
    
    // MARK: - //
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
