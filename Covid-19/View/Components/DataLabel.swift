//
//  DataLabel.swift
//  Covid-19
//
//  Created by Alwyn Yeo on 27/03/2020.
//  Copyright © 2020 Alwyn Dev. All rights reserved.
//

import UIKit

// MARK: - Class
class DataLabel: UILabel {
    
    // MARK: - Declarations (Properties)
    
    // MARK: - Declarations (UI)
    
    // MARK: - Overrides
    init(backgroundColor: UIColor? = UIColor()) {
        super.init(frame: CGRect.zero)
        setupView()
        self.backgroundColor = backgroundColor
    }
    
    // MARK: - Methods (UI)
    private func setupView() {
        layer.masksToBounds = true
        layer.cornerRadius = 10
        font = UIFont.systemFont(ofSize: 30, weight: .heavy)
        textAlignment = .center
        numberOfLines = 0
        clipsToBounds = true
        adjustsFontSizeToFitWidth = true
        translatesAutoresizingMaskIntoConstraints = false
    }
    
    // MARK: - Methods (Functions)
   
    // MARK: - //
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
