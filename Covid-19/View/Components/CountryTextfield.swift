//
//  CountryTextfield.swift
//  Covid-19
//
//  Created by Alwyn Yeo on 29/03/2020.
//  Copyright © 2020 Alwyn Dev. All rights reserved.
//

import UIKit

// MARK: - Class
class CountryTextfield: UITextField {
    
    // MARK: - Declarations (Properties)
    
    // MARK: - Declarations (UI)
    
    // MARK: - Overrides
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    override func caretRect(for position: UITextPosition) -> CGRect {
        return .zero
    }
    
    override func selectionRects(for range: UITextRange) -> [UITextSelectionRect] {
        return []
    }
    
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        let editActions = UIResponderStandardEditActions.self
        if action == #selector(editActions.copy(_:)) || action == #selector(editActions.selectAll(_:)) || action == #selector(editActions.paste(_:)) {
            return false
        }
        return super.canPerformAction(action, withSender: sender)
    }
    
    // MARK: - Methods (UI)
    private func setupView() {
        textColor = .label
        textAlignment = .center
        adjustsFontSizeToFitWidth = true
        font = .boldSystemFont(ofSize: 20)
        layer.cornerRadius = 6
        layer.borderWidth = 0.2
        layer.borderColor = UIColor.separator.cgColor
        clipsToBounds = true
        translatesAutoresizingMaskIntoConstraints = false
    }
    
    // MARK: - Methods (Functions)
    func setDefaultText(default text: String) {
        self.text = text
    }
   
    // MARK: - //
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
