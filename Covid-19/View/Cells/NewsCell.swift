//
//  NewsCell.swift
//  Covid-19
//
//  Created by Alwyn Yeo on 25/03/2020.
//  Copyright © 2020 Alwyn Dev. All rights reserved.
//

import UIKit
import SDWebImage

// MARK: - Class
class NewsCell: UICollectionViewCell {
    
    // MARK: - Declarations (Properties)
    private var news: NewsItem? {
        didSet {
            guard let news = self.news else { Errors.error(in: "NewsCell", at: #line); return }
            configureNewsData(news: news)
        }
    }
    
    // MARK: - Declarations (UI)
    fileprivate let providerLabel: UILabel = {
        let label = UILabel()
        label.font = .boldSystemFont(ofSize: 15)
        label.textAlignment = .left
        label.numberOfLines = 1
        label.lineBreakMode = .byTruncatingTail
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    fileprivate let titleLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.font = .boldSystemFont(ofSize: 18)
        label.numberOfLines = 2
        label.lineBreakMode = .byTruncatingTail
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    fileprivate let thumbnailImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleToFill
        imageView.layer.cornerRadius = 18
        imageView.clipsToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    fileprivate let dateLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 13)
        label.textAlignment = .left
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    fileprivate let separatorView: UIView = {
        let view = UIView()
        view.backgroundColor = .separator
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    // MARK: - Overrides
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    // MARK: - Methods (UI)
    private func setupView() {
        setupThumbnailImageView()
        setupProviderLabel()
        setupDateLabel()
        setupTitleLabel()
        setupSeparatorView()
    }
    
    private func setupProviderLabel() {
        addSubview(providerLabel)
        NSLayoutConstraint.activate([
            providerLabel.topAnchor.constraint(equalTo: thumbnailImageView.safeAreaLayoutGuide.topAnchor),
            providerLabel.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: 22),
            providerLabel.trailingAnchor.constraint(equalTo: thumbnailImageView.safeAreaLayoutGuide.leadingAnchor,
                                                 constant: -12),
        ])
    }
    
    private func setupTitleLabel() {
        addSubview(titleLabel)
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: providerLabel.safeAreaLayoutGuide.bottomAnchor,
                                            constant: 0),
            titleLabel.leadingAnchor.constraint(equalTo: providerLabel.safeAreaLayoutGuide.leadingAnchor),
            titleLabel.trailingAnchor.constraint(equalTo: thumbnailImageView.safeAreaLayoutGuide.leadingAnchor,
                                                 constant: -12),
            titleLabel.bottomAnchor.constraint(equalTo: dateLabel.safeAreaLayoutGuide.topAnchor),
        ])
    }
    
    private func setupDateLabel() {
        addSubview(dateLabel)
        NSLayoutConstraint.activate([
            dateLabel.leadingAnchor.constraint(equalTo: providerLabel.safeAreaLayoutGuide.leadingAnchor),
            dateLabel.trailingAnchor.constraint(equalTo: thumbnailImageView.safeAreaLayoutGuide.leadingAnchor,
                                                constant: -12),
            dateLabel.bottomAnchor.constraint(equalTo: thumbnailImageView.safeAreaLayoutGuide.bottomAnchor),
        ])
    }
    
    private func setupThumbnailImageView() {
        addSubview(thumbnailImageView)
        NSLayoutConstraint.activate([
            thumbnailImageView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor,
                                                         constant: -22),
            thumbnailImageView.centerYAnchor.constraint(equalTo: safeAreaLayoutGuide.centerYAnchor),
            thumbnailImageView.widthAnchor.constraint(equalToConstant: 100),
            thumbnailImageView.heightAnchor.constraint(equalToConstant: 100),
        ])
    }
    
    private func setupSeparatorView() {
        addSubview(separatorView)
        NSLayoutConstraint.activate([
            separatorView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: 22),
            separatorView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -22),
            separatorView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor),
            separatorView.heightAnchor.constraint(equalToConstant: 0.26),
        ])
    }
    
    // MARK: - Methods (Functions)
    func configure(newsItems news: NewsItem) {
        self.news = news
    }
    
    private func configureNewsData(news: NewsItem) {
        let newsProvider = news.provider
        guard let datePublished = news.datePublished else { Errors.error(in: "NewsCell", at: #line)
            return
        }
        guard let date = Date.getConvertedDate(dateString: datePublished, format: "MMM d, yyy") else {
            Errors.error(in: "NewsCell", at: #line)
            return
        }
        let thumbailString = news.image?.thumbnail?.contentUrl ?? ""
        
        dateLabel.text = "• \(date)"
        titleLabel.text = news.name
        
        if let thumbnailUrl = URL(string: thumbailString) {
            thumbnailImageView.sd_setImage(with: thumbnailUrl)
        } else {
            thumbnailImageView.image = #imageLiteral(resourceName: "news")
        }
        
        newsProvider.forEach { [weak self] (provider) in

            guard let self = self else { Errors.error(in: "NewsCell", at: #line); return }
            self.providerLabel.text = (provider.name ?? "Unavailable")
            
        }
    }
    
    // MARK: - //
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
