//
//  RegionCell.swift
//  Covid-19
//
//  Created by Alwyn Yeo on 30/06/2020.
//  Copyright © 2020 Alwyn Dev. All rights reserved.
//

import UIKit

// MARK: - Class
class RegionCell: UITableViewCell {
    
    // MARK: - Declarations (Properties)
    private var region: Region? {
        didSet {
            guard let region = self.region else { Errors.error(in: "RegionCell", at: #line); return }
            configureRegionData(region: region)
        }
    }
    
    // MARK: - Declarations (UI)
    fileprivate let regionTitleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .label
        label.font = .boldSystemFont(ofSize: 17)
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    fileprivate let checkSymbolImageView: UIImageView = {
        let imageView = UIImageView(image: #imageLiteral(resourceName: "check-filled"))
        imageView.isHidden = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    fileprivate let regionImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    // MARK: - Overrides
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
        selectionStyle = .none
        if selected {
            checkSymbolImageView.isHidden = false
        } else {
            checkSymbolImageView.isHidden = true
        }
    }
    
    // MARK: - Methods (UI)
    private func setupView() {
        setupRegionTitleLabel()
        setupCheckSymbolImageView()
        setupRegionImageView()
    }
    
    private func setupRegionTitleLabel() {
        addSubview(regionTitleLabel)
        NSLayoutConstraint.activate([
            regionTitleLabel.centerYAnchor.constraint(equalTo: safeAreaLayoutGuide.centerYAnchor),
            regionTitleLabel.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: 22),
        ])
    }
    
    private func setupCheckSymbolImageView() {
        addSubview(checkSymbolImageView)
        NSLayoutConstraint.activate([
            checkSymbolImageView.centerYAnchor.constraint(equalTo: regionTitleLabel.safeAreaLayoutGuide.centerYAnchor),
            checkSymbolImageView.leadingAnchor.constraint(equalTo: regionTitleLabel.safeAreaLayoutGuide.trailingAnchor, constant: 8),
            checkSymbolImageView.widthAnchor.constraint(equalToConstant: 24),
            checkSymbolImageView.heightAnchor.constraint(equalToConstant: 24),
        ])
    }
    
    private func setupRegionImageView() {
        addSubview(regionImageView)
        NSLayoutConstraint.activate([
            regionImageView.centerYAnchor.constraint(equalTo: regionTitleLabel.safeAreaLayoutGuide.centerYAnchor),
            regionImageView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor,
                                                      constant: -22),
            regionImageView.widthAnchor.constraint(equalToConstant: 40),
            regionImageView.heightAnchor.constraint(equalToConstant: 40),
        ])
    }
    
    // MARK: - Methods (Functions)
    func setRegion(region: Region) {
        self.region = region
    }
    
    private func configureRegionData(region: Region) {
        regionTitleLabel.text = region.title
        regionImageView.image = UIImage(named: region.image ?? "us-60")
    }
    
    // MARK: - //
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
