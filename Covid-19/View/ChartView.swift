//
//  ChartView.swift
//  Covid-19
//
//  Created by Alwyn Yeo on 02/04/2020.
//  Copyright © 2020 Alwyn Dev. All rights reserved.
//

import UIKit
import Charts

// MARK: - Class
class ChartView: UIView {
    
    // MARK: - Declarations (Properties)
    fileprivate var totalCasesDataChart = PieChartDataEntry(value: 0)
    fileprivate var recoveredCasesDataChart = PieChartDataEntry(value: 0)
    fileprivate var totalDeathsDataChart = PieChartDataEntry(value: 0)
    fileprivate var pieCharts = [ChartDataEntry]()
    
    // MARK: - Declarations (UI)
    fileprivate let pieChartsView: PieChartView = {
        let view = PieChartView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    // MARK: - Overrides
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    // MARK: - Methods (UI)
    private func setupView() {
        isHidden = true
        translatesAutoresizingMaskIntoConstraints = false
        setupPieChartsView()
    }
    
    private func setupPieChartsView() {
        addSubview(pieChartsView)
        NSLayoutConstraint.activate([
            pieChartsView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
            pieChartsView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: 8),
            pieChartsView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -8),
            pieChartsView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: -12),
        ])
    }
    
    // MARK: - Methods (Functions)
    func setPieChartDataEntry(total: Double, deaths: Double, recovered: Double) {
        totalCasesDataChart = PieChartDataEntry(value: total)
        totalDeathsDataChart = PieChartDataEntry(value: deaths)
        recoveredCasesDataChart = PieChartDataEntry(value: recovered)
        
        pieCharts = [
            totalCasesDataChart,
            totalDeathsDataChart,
            recoveredCasesDataChart,
        ]
        
        totalCasesDataChart.label = "Total"
        totalDeathsDataChart.label = "Deaths"
        recoveredCasesDataChart.label = "Recovered"
        
        let chartDataSet = PieChartDataSet(entries: pieCharts, label: nil)
        let chartData = PieChartData(dataSet: chartDataSet)
        
        let colors = [UIColor.systemBlue, UIColor.systemRed, UIColor.systemGreen]
        chartDataSet.colors = colors
        
        DispatchQueue.main.async {
            self.pieChartsView.data = chartData
        }
    }
   
    // MARK: - //
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
