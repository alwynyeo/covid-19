//
//  NewsFooterView.swift
//  Covid-19
//
//  Created by Alwyn Yeo on 07/11/2020.
//  Copyright © 2020 Alwyn Dev. All rights reserved.
//

import UIKit
import Network

// MARK: - Class
class NewsFooterView: UICollectionReusableView {
    // MARK: - Declarations (Properties)
    // MARK: - Declarations (UI)
    fileprivate let activityIndicatorView: UIActivityIndicatorView = {
        let view = UIActivityIndicatorView(style: .medium)
        view.startAnimating()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    fileprivate let networkInformationLabel: UILabel = {
        let label = UILabel()
        let firstLineText = "Couldn't Load News"
        let secondLineText = "Covid-19 isn't connected to the Internet."
        
        let color = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        let firstLineAttributes: [NSAttributedString.Key: Any] = [
            .font: UIFont.boldSystemFont(ofSize: 17),
            .foregroundColor: color,
        ]
        let secondLineAttributes: [NSAttributedString.Key: Any] = [
            .font: UIFont.systemFont(ofSize: 17),
            .foregroundColor: color,
        ]
        
        let attributedText = NSMutableAttributedString(string: firstLineText, attributes: firstLineAttributes)
        let spacing = NSAttributedString(string: "\n")
        let secondLine = NSAttributedString(string: secondLineText, attributes: secondLineAttributes)
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 5
        
        attributedText.append(spacing)
        attributedText.append(secondLine)
        attributedText.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: attributedText.length))
        
        label.attributedText = attributedText
        label.textAlignment = .center
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    // MARK - Overrides
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    // MARK: - Methods (UI)
    private func setupView() {
        configureActivityIndicatorView()
        configureNetworkInformationLabel()
        monitorNetworkChangeObserver()
    }
    
    private func configureActivityIndicatorView() {
        addSubview(activityIndicatorView)
        NSLayoutConstraint.activate([
            activityIndicatorView.centerXAnchor.constraint(equalTo: safeAreaLayoutGuide.centerXAnchor),
            activityIndicatorView.centerYAnchor.constraint(equalTo: safeAreaLayoutGuide.centerYAnchor),
        ])
    }
    
    private func configureNetworkInformationLabel() {
        addSubview(networkInformationLabel)
        NSLayoutConstraint.activate([
            networkInformationLabel.centerXAnchor.constraint(equalTo: safeAreaLayoutGuide.centerXAnchor),
            networkInformationLabel.centerYAnchor.constraint(equalTo: safeAreaLayoutGuide.centerYAnchor),
        ])
    }
    // MARK: - Methods (Functions)
    private func monitorNetworkChangeObserver() {
        let monitor = NWPathMonitor()
        
        monitor.pathUpdateHandler = { [weak self] (path) in
            guard let self = self else { Errors.error(in: "NewsFooterView", at: #line); return }
            let status = path.status
            
            if status == .satisfied {
                DispatchQueue.main.async {
                    self.activityIndicatorView.startAnimating()
                    self.networkInformationLabel.isHidden = true
                }
            } else if status == .unsatisfied {
                DispatchQueue.main.async {
                    self.activityIndicatorView.stopAnimating()
                    self.networkInformationLabel.isHidden = false
                }
            } else {
                DispatchQueue.main.async {
                    self.activityIndicatorView.stopAnimating()
                    self.networkInformationLabel.isHidden = false
                }
            }
        }
        monitor.start(queue: .global())
    }
    
    // MARK: - deinit
    deinit {
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
