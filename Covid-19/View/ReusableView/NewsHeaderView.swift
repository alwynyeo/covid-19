//
//  NewsHeaderView.swift
//  Covid-19
//
//  Created by Alwyn Yeo on 07/11/2020.
//  Copyright © 2020 Alwyn Dev. All rights reserved.
//

import UIKit

// MARK: - Protocol
protocol NewsHeaderViewProtocol {
    func didPressCountryButton()
}

// MARK: - Class
class NewsHeaderView: UICollectionReusableView {
    // MARK: - Declarations (Properties)
    var delegate: NewsHeaderViewProtocol?
    var newsViewController: NewsViewController? {
        didSet {
            newsViewController?.delegate = self
        }
    }
    
    // MARK: - Declarations (UI)
    fileprivate let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "News"
        label.font = UIFont.boldSystemFont(ofSize: 34)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    fileprivate lazy var countryButton: UIButton = {
        let button = UIButton(type: .system)
        button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        button.setImage(UIImage(named: "\(defaults.string(forKey: globalRegionCodeKey)?.lowercased() ?? "us")-30"), for: .normal)
        button.layer.cornerRadius = button.bounds.size.width / 2
        button.clipsToBounds = true
        button.addTarget(self, action: #selector(handleCountryButton(_:)), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    fileprivate let separatorView: UIView = {
        let view = UIView()
        view.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    // MARK: - Overrides
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    // MARK: - Methods (UI)
    private func setupView() {
        configureTitleLabel()
        configureCountryButton()
//        configureSeparatorView()
    }
    
    private func configureTitleLabel() {
        addSubview(titleLabel)
        NSLayoutConstraint.activate([
            titleLabel.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: 22),
            titleLabel.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: -10),
        ])
    }
    
    private func configureCountryButton() {
        addSubview(countryButton)
        NSLayoutConstraint.activate([
            countryButton.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 5),
            countryButton.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -22),
        ])
    }
    
    private func configureSeparatorView() {
        addSubview(separatorView)
        NSLayoutConstraint.activate([
            separatorView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: 22),
            separatorView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -22),
            separatorView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor),
            separatorView.heightAnchor.constraint(equalToConstant: 0.2),
        ])
    }
    
    // MARK: - Methods (Functions)
    func configureData(image: UIImage) {
        countryButton.setImage(image, for: .normal)
    }
    
    // MARK: - Objc Methods
    @objc private func handleCountryButton(_ sender: UIButton?) {
        delegate?.didPressCountryButton()
    }
    
    // MARK: - //
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

// MARK: - Extension
extension NewsHeaderView {}

extension NewsHeaderViewProtocol {
    func didPressCountryButton() {}
}

extension NewsHeaderView: NewsViewControllerProtocol {
    func getRegionNewsResults(region code: String) {
        countryButton.setImage(UIImage(named: "\(code)-30"), for: .normal)
    }
}
