//
//  CardView.swift
//  Covid-19
//
//  Created by Alwyn Yeo on 26/03/2020.
//  Copyright © 2020 Alwyn Dev. All rights reserved.
//

import UIKit

// MARK: - Class
class CardView: UIView {
    
    // MARK: - Declarations (Properties)
    weak var homeViewController: HomeViewController? {
        didSet {
            homeViewController?.delegate = self
        }
    }
    private lazy var contentViewSize = CGSize(width: frame.width, height: frame.height + 530)
    
    // MARK: - Declarations (UI)
    fileprivate lazy var totalLabel = DataLabel(backgroundColor: .systemBlue)
    fileprivate lazy var newLabel = DataLabel(backgroundColor: .systemPink)
    fileprivate lazy var activeLabel = DataLabel(backgroundColor: .systemYellow)
    fileprivate lazy var criticalLabel = DataLabel(backgroundColor: .systemRed)
    fileprivate lazy var recoveredLabel = DataLabel(backgroundColor: .systemGreen)
    fileprivate lazy var newDeathsLabel = DataLabel(backgroundColor: .systemPink)
    fileprivate lazy var deathsLabel = DataLabel(backgroundColor: .systemRed)
    fileprivate let floatingView = FloatingView()
    var isDataEmpty = true
    
    fileprivate lazy var scrollView: UIScrollView = {
        let view = UIScrollView(frame: .zero)
        view.frame = self.bounds
        view.contentSize = contentViewSize
        view.autoresizingMask = .flexibleHeight
        view.showsVerticalScrollIndicator = true
        view.showsHorizontalScrollIndicator = false
        view.bounces = true
        return view
    }()
    
    fileprivate lazy var containerView: UIView = {
        let view = UIView()
        view.frame.size = contentViewSize
        return view
    }()
    
    fileprivate lazy var stackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [
            totalLabel,
            newLabel,
            activeLabel,
            criticalLabel,
            recoveredLabel,
            newDeathsLabel,
            deathsLabel,
        ])
        stackView.distribution = .equalCentering
        stackView.spacing = 0
        stackView.axis = .vertical
        stackView.alignment = .center
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    // MARK: - Overrides
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    // MARK: - Methods (UI)
    private func setupView() {
        translatesAutoresizingMaskIntoConstraints = false
        setupScrollView()
        setupStackView()
        setupStackViewLabels()
        configureFloatingView()
    }
    
    private func setupScrollView() {
        addSubview(scrollView)
        scrollView.addSubview(containerView)
    }
    
    private func setupStackView() {
        scrollView.addSubview(stackView)
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: containerView.safeAreaLayoutGuide.topAnchor),
            stackView.leadingAnchor.constraint(equalTo: containerView.safeAreaLayoutGuide.leadingAnchor,
                                               constant: 18),
            stackView.trailingAnchor.constraint(equalTo: containerView.safeAreaLayoutGuide.trailingAnchor,
                                                constant: -18),
            stackView.bottomAnchor.constraint(equalTo: containerView.safeAreaLayoutGuide.bottomAnchor,
                                              constant: -28),
        ])
    }
    
    private func setupStackViewLabels() {
        NSLayoutConstraint.activate([
            totalLabel.leadingAnchor.constraint(equalTo: stackView.safeAreaLayoutGuide.leadingAnchor),
            totalLabel.trailingAnchor.constraint(equalTo: stackView.safeAreaLayoutGuide.trailingAnchor),
            totalLabel.heightAnchor.constraint(equalToConstant: 155),
            newLabel.leadingAnchor.constraint(equalTo: stackView.safeAreaLayoutGuide.leadingAnchor),
            newLabel.trailingAnchor.constraint(equalTo: stackView.safeAreaLayoutGuide.trailingAnchor),
            newLabel.heightAnchor.constraint(equalToConstant: 155),
            activeLabel.leadingAnchor.constraint(equalTo: stackView.safeAreaLayoutGuide.leadingAnchor),
            activeLabel.trailingAnchor.constraint(equalTo: stackView.safeAreaLayoutGuide.trailingAnchor),
            activeLabel.heightAnchor.constraint(equalToConstant: 155),
            criticalLabel.leadingAnchor.constraint(equalTo: stackView.safeAreaLayoutGuide.leadingAnchor),
            criticalLabel.trailingAnchor.constraint(equalTo: stackView.safeAreaLayoutGuide.trailingAnchor),
            criticalLabel.heightAnchor.constraint(equalToConstant: 155),
            recoveredLabel.leadingAnchor.constraint(equalTo: stackView.safeAreaLayoutGuide.leadingAnchor),
            recoveredLabel.trailingAnchor.constraint(equalTo: stackView.safeAreaLayoutGuide.trailingAnchor),
            recoveredLabel.heightAnchor.constraint(equalToConstant: 155),
            newDeathsLabel.leadingAnchor.constraint(equalTo: stackView.safeAreaLayoutGuide.leadingAnchor),
            newDeathsLabel.trailingAnchor.constraint(equalTo: stackView.safeAreaLayoutGuide.trailingAnchor),
            newDeathsLabel.heightAnchor.constraint(equalToConstant: 155),
            deathsLabel.leadingAnchor.constraint(equalTo: stackView.safeAreaLayoutGuide.leadingAnchor),
            deathsLabel.trailingAnchor.constraint(equalTo: stackView.safeAreaLayoutGuide.trailingAnchor),
            deathsLabel.heightAnchor.constraint(equalToConstant: 155),
        ])
    }
    
    private func configureFloatingView() {
        scrollView.addSubview(floatingView)
        NSLayoutConstraint.activate([
            floatingView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: 16),
            floatingView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -16),
            floatingView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: 90),
            floatingView.heightAnchor.constraint(equalToConstant: 50),
        ])
    }
    
    // MARK: - Methods (Functions)
    func configure(total: Int, new: Int, active: Int, critical: Int, recovered: Int, newDeaths: Int, deaths: Int) {
        let totalCases = formattedString(data: total)
        let newCases = formattedString(data: new)
        let activeCases = formattedString(data: active)
        let criticalCases = formattedString(data: critical)
        let recoveredCases = formattedString(data: recovered)
        let newDeathCases = formattedString(data: newDeaths)
        let deathCases = formattedString(data: deaths)
        
        let titleAttributes: [NSAttributedString.Key : Any] = [
            NSAttributedString.Key.font : UIFont.systemFont(ofSize: 20, weight: .heavy),
            NSAttributedString.Key.foregroundColor : #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1),
        ]
        let dataAttributes: [NSAttributedString.Key : Any] = [
            NSAttributedString.Key.font : UIFont.systemFont(ofSize: 36, weight: .heavy),
            NSAttributedString.Key.foregroundColor : #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        ]
        
        let totalAttributedText = NSMutableAttributedString(string: "TOTAL", attributes: titleAttributes)
        let newAttributedText = NSMutableAttributedString(string: "NEW", attributes: titleAttributes)
        let activeAttributedText = NSMutableAttributedString(string: "ACTIVE", attributes: titleAttributes)
        let criticalAttributedText = NSMutableAttributedString(string: "CRITICAL", attributes: titleAttributes)
        let recoveredAttributedText = NSMutableAttributedString(string: "RECOVERED", attributes: titleAttributes)
        let newDeathsAttributedText = NSMutableAttributedString(string: "NEW DEATHS", attributes: titleAttributes)
        let deathsAttributedText = NSMutableAttributedString(string: "DEATHS", attributes: titleAttributes)
        
        let spacing = NSAttributedString(string: "\n\n")
        
        let totalDataAttributedText = NSAttributedString(string: totalCases, attributes: dataAttributes)
        let newDataAttributedText = NSAttributedString(string: "+\(newCases)", attributes: dataAttributes)
        let activeDataAttributedText = NSAttributedString(string: activeCases, attributes: dataAttributes)
        let criticalDataAttributedText = NSAttributedString(string: criticalCases, attributes: dataAttributes)
        let recoveredDataAttributedText = NSAttributedString(string: recoveredCases, attributes: dataAttributes)
        let newDeathsDataAttributedText = NSAttributedString(string: "+\(newDeathCases)", attributes: dataAttributes)
        let deathsDataAttributedText = NSAttributedString(string: deathCases, attributes: dataAttributes)
        
        totalAttributedText.append(spacing)
        totalAttributedText.append(totalDataAttributedText)
        newAttributedText.append(spacing)
        newAttributedText.append(newDataAttributedText)
        activeAttributedText.append(spacing)
        activeAttributedText.append(activeDataAttributedText)
        criticalAttributedText.append(spacing)
        criticalAttributedText.append(criticalDataAttributedText)
        recoveredAttributedText.append(spacing)
        recoveredAttributedText.append(recoveredDataAttributedText)
        newDeathsAttributedText.append(spacing)
        newDeathsAttributedText.append(newDeathsDataAttributedText)
        deathsAttributedText.append(spacing)
        deathsAttributedText.append(deathsDataAttributedText)
        
        DispatchQueue.main.async {
            self.totalLabel.attributedText = totalAttributedText
            self.newLabel.attributedText = newAttributedText
            self.activeLabel.attributedText = activeAttributedText
            self.criticalLabel.attributedText = criticalAttributedText
            self.recoveredLabel.attributedText = recoveredAttributedText
            self.newDeathsLabel.attributedText = newDeathsAttributedText
            self.deathsLabel.attributedText = deathsAttributedText
        }
        isDataEmpty = false
    }
    
    private func animateFloatingView() {
        if floatingView.transform == .identity {
            let y: CGFloat = -90 - 10
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                UIView.animate(withDuration: 0.7, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.7, options: .curveEaseOut) {
                    self.floatingView.transform = .init(translationX: 0, y: y)
                } completion: { [weak self] (success) in
                    guard let self = self else {
                        Errors.error(in: "CardView", at: #line)
                        return
                    }
                    guard success == true else {
                        Errors.error(in: "CardView", at: #line)
                        return
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                        UIView.animate(withDuration: 0.7, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.7, options: .curveEaseOut) {
                            self.floatingView.transform = .identity
                        }
                    }
                }
            }
        }
    }
    
    // MARK: - //
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

extension CardView: HomeViewControllerProtocol {
    func getResponseMessage(network: Network) {
        floatingView.configure(network: network)
        if network == .failed || network == .unidentified {
            animateFloatingView()
        }
    }
}
